# spotwrite

spotwrite 用のプロジェクトリポジトリです。

## Requirement

- PHP 7.4+
- Docker
- Docker Compose 

## Setup

git リポジトリを clone します。
```shell
$ git clone git@bitbucket.org:worksmedia/spotwrite-api.git
$ cd spotwrite-api
```

docker-compose.yml 用の `.env` 、 Laravel 用の `.env` をコピーします。
```shell
$ cp .env.example .env
$ cp src/.env.example src/.env
```

docker をビルドして起動します。
```shell
$ make up
```

以下のコマンドを実行してセットアップを完了させます
```shell
$ make app-init
```

また、以下のコマンドを実行すると Phpstorm で開発をする際に色々と便利になります。
```shell
$ make app-ide-helper
```

## Update

git pull や fetch などを行いローカルのリポジトリを最新化した場合は以下のコマンドを実行して composer の更新を行います。
```shell
$ make app-composer-install
```

## tips

- ブランチを切り替えたあとなど、 Seeder の流し込みや aritsan がうまくとおらない場合は下記のコマンドを実行してみてください

```shell
$ make app-clear
```

# ドキュメント

## apiの確認

`make up` コマンドでコンテナを起動したに http://localhost:8080 にアクセスする。

## dbdoc の確認

```sh
$ open ./document/dbdoc/README.md
```

## dbdoc の更新方法

下記のコマンドを実行します（ローカルのデータがすべて消えます）。

```sh
$ make doc-db-update
```
