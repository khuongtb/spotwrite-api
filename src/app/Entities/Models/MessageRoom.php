<?php

namespace App\Entities\Models;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Traits\UuidTrait;

/**
 * Class MessageRoom
 * @package App\Entities\Models
 */
class MessageRoom extends Model
{
    use UuidTrait;

    /**
     * @var string
     */
    protected $table = 'message_rooms';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
