<?php

namespace App\Entities\Models;

use App\Entities\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationCategory
 * @package App\Entities\Models
 */
class OrganizationCategory extends Model
{
    use UuidTrait;

    /**
     * @var string[]
     */
    protected $fillable = [
        'company_id',
        'name',
        'description',
    ];
}
