<?php

namespace App\Entities\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MessageRoomUser
 * @package App\Entities\Models
 */
class MessageRoomUser extends Model
{
    /**
     * @var string
     */
    protected $table = 'message_room_user';
}
