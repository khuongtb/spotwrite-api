<?php

namespace App\Providers;

use App\Entities\Models\UserAuthentication;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Support\Arrayable;
use Str;

/**
 * users の認証に必要な情報（email, password）が user_authentications に含まれているため、
 * user_authentications で認証を試みるようにするためのサービスプロバイダ
 *
 * Class CustomEloquentUserProvider
 * @package App\Providers
 */
class CustomEloquentUserProvider extends EloquentUserProvider
{
    /**
     * @param array $credentials
     * @return \Illuminate\Contracts\Auth\Authenticatable|\Illuminate\Database\Eloquent\HigherOrderBuilderProxy|mixed|null
     */
    public function retrieveByCredentials(array $credentials)
    {
        if (
            empty($credentials) ||
            (count($credentials) === 1 &&
                Str::contains($this->firstCredentialKey($credentials), 'password'))
        ) {
            return null;
        }

        $query = UserAuthentication::query();

        /** @var string $key **/
        foreach ($credentials as $key => $value) {
            if (Str::contains($key, 'password')) {
                continue;
            }

            if (is_array($value) || $value instanceof Arrayable) {
                $query->whereIn($key, $value);
            } else {
                $query->where($key, $value);
            }
        }

        /** @var UserAuthentication $userAuth */
        $userAuth = $query->first();

        return $userAuth->user ?? null;
    }
}
