<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use App\Entities\Models\Message;
use App\Entities\Models\MessageRoom;
use App\Events\MessageReceived;
use Illuminate\Http\Request;
use Str;
use Carbon\Carbon;
use App\Http\Requests\User\Message\StoreTextRequest;
use App\Http\Requests\User\Message\StoreRogerRequest;
use App\Http\Requests\User\Message\StoreImagesRequest;
use App\Http\Requests\User\Message\StoreFilesRequest;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

/**
 * Class MessageController
 * @package App\Http\Controllers
 */
class MessageController extends Controller
{
    /**
     * @param StoreTextRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeText(StoreTextRequest $request)
    {
        $message = Message::create([
            'chat_message_identifier' => "user_name" . "#" . Carbon::now()->format('Y-m-d:H:m:s'),
            'room_uuid' => $request->input('room_uuid'),
            'body' => json_encode([
                'user_uuid' => auth()->user()->uuid,
                'type' => 'text',
                'content' => $request->input('message'),
            ])
        ]);

        event(new MessageReceived($message->body, $message->room_uuid));

        return response()->json();
    }

    /**
     * @param StoreRogerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeRoger(StoreRogerRequest $request)
    {
        $message = Message::create([
            'chat_message_identifier' => "user_name" . "#" . Carbon::now()->format('Y-m-d:H:m:s'),
            'room_uuid' => $request->input('room_uuid'),
            'body' => json_encode([
                'user_uuid' => auth()->user()->uuid,
                'type' => 'roger',
            ])
        ]);

        event(new MessageReceived($message->body, $message->room_uuid));

        return response()->json();
    }

    /**
     * @param StoreImagesRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeImages(StoreImagesRequest $request)
    {
        $message = Message::create([
            'chat_message_identifier' => "user_name" . "#" . Carbon::now()->format('Y-m-d:H:m:s'),
            'room_uuid' => $request->input('room_uuid'),
            'body' => json_encode([
                'user_uuid' => auth()->user()->uuid,
                'type' => 'images',
                'content' => $request->input('images'),
            ])
        ]);

        event(new MessageReceived($message->body, $message->room_uuid));

        return response()->json();
    }

    /**
     * @param StoreFilesRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeFiles(StoreFilesRequest $request)
    {
        $message = Message::create([
            'chat_message_identifier' => "user_name" . "#" . Carbon::now()->format('Y-m-d:H:m:s'),
            'room_uuid' => $request->input('room_uuid'),
            'body' => json_encode([
                'user_uuid' => auth()->user()->uuid,
                'type' => 'files',
                'content' => $request->input('files'),
            ])
        ]);

        event(new MessageReceived($message->body, $message->room_uuid));

        return response()->json();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllMessages()
    {
        $model = new Message();
        $messages = $model->all();

        return response()->json(['messages' => $messages]);
    }
}
