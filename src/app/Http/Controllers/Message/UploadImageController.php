<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Http\Requests\User\Message\UploadImageRequest;
use Illuminate\Http\JsonResponse;

/**
 * Class MessageController
 * @package App\Http\Controllers
 */
class UploadImageController extends Controller
{
    /***
     * @param UploadImageRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UploadImageRequest $request): JsonResponse
    {
        $file = $request->file('image');

        // 保存先ディレクトリ作成
        $companyId = auth()->user()->company->uuid;
        $fileDir = $companyId . '/img';

        return response()->json([
            'thumbnail_url' => $this->createThumbnailImageUrl($fileDir, $file),
            'main_image_url' => $this->createMainImageUrl($fileDir, $file),
        ]);
    }

    /***
     * @param string $fileDir
     * @param $file
     * @return string
     */
    private function createThumbnailImageUrl(string $fileDir, $file): string
    {
        // リサイズ画像の一時保存先
        $now = Carbon::now()->format('YmdHis');
        $tmpFile = 'tmp-' . $now;
        $tmpPath = storage_path('app/tmp/' . $tmpFile);

        // リサイズ
        $image = Image::make($file)->resize(300, 300);
        $image->save($tmpPath);

        // リサイズした画像を保存
        $imagePath = Storage::disk()
            ->putFile($fileDir, $tmpPath, ['public']);

        // 一時保存した画像削除
        Storage::disk('local')->delete('tmp/' . $tmpFile);

        // URL取得
        return Storage::disk()->url((string) $imagePath);
    }

    /**
     * @param string $fileDir
     * @param mixed $file
     * @return string
     */
    private function createMainImageUrl(string $fileDir, $file): string
    {
        // リサイズ画像の一時保存先
        $now = Carbon::now()->format('YmdHis');
        $tmpFile = 'tmp-' . $now;
        $path = storage_path('app/tmp/' . $tmpFile);

        $quality = 80;

        // 1MB まで圧縮。この値は仮。
        $byte = 1024000;

        $this->compressImage($file, $path, $quality, $byte);

        $imagePath = Storage::disk()->putFile($fileDir, $path, ['public']);

        // 一時保存した画像削除
        Storage::disk('local')->delete('tmp/' . $tmpFile);

        return Storage::disk()->url((string) $imagePath);
    }

    /**
     * @param mixed $file
     * @param string $path
     * @param int $quality
     * @param int $size
     * @return mixed
     */
    private function compressImage($file, string $path, int $quality, int $size)
    {
        $image = Image::make($file);
        $image->save($path, $quality);

        // クオリティを下げた後のファイルサイズが十分下がっていなければもう一度関数を呼び出す
        $image = Image::make($path);
        if ($image->filesize() > $size) {
            $quality -= 10;
            return $this->compressImage($file, $path, $quality, $size);
        }
    }
}
