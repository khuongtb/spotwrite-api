<?php

namespace App\Http\Controllers\Message;

use App\Http\Controllers\Controller;
use App\Entities\Models\User;
use App\Entities\Models\MessageRoom;
use Illuminate\Http\Request;
use App\Http\Requests\User\Message\RoomRequest;

/**
 * Class RoomController
 * @package App\Http\Controllers\Message
 */
class RoomController extends Controller
{
    /**
     * @param RoomRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RoomRequest $request)
    {
        $userUuids = $request->input('user_uuids');
        $messageRoom = MessageRoom::create();
        $userIds = [];
        foreach ($userUuids as $userUuid) {
            $userIds[] = User::getIdByUuid($userUuid);
        }

        $messageRoom->users()->attach($userIds);

        return response()->json([
            'room_uuid' => $messageRoom->uuid,
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = auth()->user();
        $rooms = $user->messageRooms->map(function ($item) {
            return ['room_uuid' => $item->uuid];
        });

        return response()->json([
            'rooms' => $rooms
        ]);
    }
}
