<?php

namespace App\Http\Controllers\Company;

use App\Entities\Models\CompanyFile;
use App\Entities\Models\Organization;
use App\Entities\Models\OrganizationCategory;
use App\Entities\Models\OrganizationClosure;
use App\Entities\Models\OrganizationImage;
use App\Http\Controllers\Controller;
use App\Http\Requests\Company\Organizations\CreateRequest;
use App\Http\Requests\Company\Organizations\IndexRequest;
use App\Http\Responses\Company\Organizations\CreateResponse;
use App\Http\Responses\Company\Organizations\IndexResponse;
use App\Http\Responses\Company\Organizations\ShowResponse;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

/**
 * Class OrganizationsController
 * @package App\Http\Controllers\Company
 */
class OrganizationsController extends Controller
{
    /**
     * @param IndexRequest $request
     * @return JsonResponse
     */
    public function index(IndexRequest $request): JsonResponse
    {
        $accountHolder = auth()->user();

        // TODO: 検索の処理を入れる
        $request->all(); // TODO: phpmd で変数使ってないと怒られるので仮で置いておく

        return response()->json(
            IndexResponse::body($accountHolder->company->organizations),
            Response::HTTP_OK
        );
    }

    /**
     * @param Organization $organization
     * @return JsonResponse
     */
    public function show(Organization $organization): JsonResponse
    {
        return response()->json(ShowResponse::body($organization));
    }

    /**
     * @param CreateRequest $request
     * @param Organization|null $organization
     * @return JsonResponse
     * @throws \Throwable
     */
    public function create(CreateRequest $request, ?Organization $organization): JsonResponse
    {
        // FIXME: 親組織に追加する場合に、その親組織の深さが1以下であることを確認する処理入れる

        $params = $request->validated();
        $company = auth()->user()->company;

        // FIXME: この一連の処理はサービスクラス化する？
        $newOrganization = DB::transaction(function () use ($params, $company, $organization) {
            $categoryId = OrganizationCategory::getIdByUuid($params['category_uuid']);
            $newOrganization = Organization::create([
                'company_id' => $company->id,
                'category_id' => $categoryId,
                'name' => $params['name'],
                'description' => $params['description'],
                'status' => $params['status'],
            ]);

            // 自身を Closure に追加する
            OrganizationClosure::create([
                'parent_id' => $newOrganization->id,
                'child_id' => $newOrganization->id,
                'depth' => 0,
            ]);

            // 親要素が指定されているのであれば、親要素とその親要素を子要素に持つ全ての要素に子要素を追加する
            if (!empty($organization->id)) {
                $closure = OrganizationClosure::select(['parent_id', 'depth'])->ofChildId($organization->id)->get();
                foreach ($closure as $item) {
                    // 閉包テーブルの構築
                    OrganizationClosure::create([
                        'parent_id' => $item->parent_id,
                        'child_id' => $newOrganization->id,
                        'depth' => $item->depth + 1,
                    ]);
                }
            }

            // ファイルが添付されていたら組織と紐付ける
            if (!empty($params['file_uuid'])) {
                $fileId = CompanyFile::getIdByUuid($params['file_uuid']);
                OrganizationImage::create([
                    'organization_id' => $newOrganization->id,
                    'company_file_id' => $fileId,
                ]);
            }

            return $newOrganization;
        });

        return response()->json(CreateResponse::body($newOrganization));
    }
}
