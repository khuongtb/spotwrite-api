<?php

namespace App\Http\Requests\Company\Organizations;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class CreateRequest
 * @package App\Http\Requests\Company\Organizations
 */
class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_uuid' => ['required', 'string'],
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'status' => ['required', 'string'],
            'file_uuid' => ['present', 'nullable', 'string'],
        ];
    }
}
