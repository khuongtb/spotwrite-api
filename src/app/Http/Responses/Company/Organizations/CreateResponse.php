<?php

namespace App\Http\Responses\Company\Organizations;

use App\Entities\Models\Organization;
use App\Http\Resources\OrganizationResource;

/**
 * Class CreateResponse
 * @package App\Http\Responses\Company\Organizations
 */
class CreateResponse
{
    /**
     * @param Organization $organizations
     * @return OrganizationResource
     */
    public static function body(Organization $organizations): OrganizationResource
    {
        return new OrganizationResource($organizations);
    }
}
