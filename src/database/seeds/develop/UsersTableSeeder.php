<?php

namespace develop;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Seeder;
use Str;

/**
 * Class UsersTableSeeder
 * @package develop
 */
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'name' => 'ユーザ01',
                'name_kana' => 'ゆーざぜろいち',
                'tel' => '000-1111-1111',
                'birthplace' => '東京都',
                'birthday' => new Carbon('2000-01-01'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'name' => 'ユーザ02',
                'name_kana' => 'ゆーざぜろに',
                'tel' => '000-1111-2222',
                'birthplace' => '東京都',
                'birthday' => new Carbon('2000-01-02'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'uuid' => Str::uuid()->toString(),
                'company_id' => 1,
                'name' => 'ユーザ03',
                'name_kana' => 'ゆーざぜろさん',
                'tel' => '000-1111-3333',
                'birthplace' => '東京都',
                'birthday' => new Carbon('2000-01-03'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);

        DB::table('user_authentications')->insert([
            [
                'id' => 1,
                'uuid' => Str::uuid()->toString(),
                'user_id' => 1,
                'email' => 'user01@example.com',
                'password' => bcrypt('password'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 2,
                'uuid' => Str::uuid()->toString(),
                'user_id' => 2,
                'email' => 'user02@example.com',
                'password' => bcrypt('password'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id' => 3,
                'uuid' => Str::uuid()->toString(),
                'user_id' => 3,
                'email' => 'user03@example.com',
                'password' => bcrypt('password'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ]);
    }
}
