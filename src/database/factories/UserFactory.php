<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Models\Company;
use App\Entities\Models\User;
use App\Entities\Models\UserAuthentication;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'uuid' => $faker->uuid,
        'company_id' => function () {
            return factory(Company::class)->create()->id;
        },
        'name' => $faker->name,
        'name_kana' => $faker->name,
        'tel' => $faker->phoneNumber,
        'birthplace' => $faker->city,
        'birthday' => $faker->date(),
        'is_active' => true,
    ];
});

$factory->afterCreatingState(User::class, 'with_authentication', function ($user) {
    $user->authentication()->save(factory(UserAuthentication::class)->make());
});
