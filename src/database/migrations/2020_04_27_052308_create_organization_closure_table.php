<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationClosureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization_closure', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->unique()->comment('UUID');
            $table->unsignedBigInteger('parent_id')->comment('親組織ID');
            $table->unsignedBigInteger('child_id')->comment('子組織ID');
            $table->unsignedInteger('depth')->comment('階層');
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('organizations');
            $table->foreign('child_id')->references('id')->on('organizations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organization_structures');
    }
}
