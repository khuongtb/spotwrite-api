<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->uuid('uuid')->unique()->comment('UUID');
            $table->unsignedBigInteger('company_id')->comment('会社ID');
            $table->string('name')->comment('氏名');
            $table->string('name_kana')->comment('ふりがな');
            $table->string('tel')->comment('電話番号');
            $table->string('birthplace')->comment('出身地');
            $table->date('birthday')->comment('誕生日');
            $table->boolean('is_active')->default(true)->comment('アクティブ状態かどうか');
            $table->timestamps();

            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
