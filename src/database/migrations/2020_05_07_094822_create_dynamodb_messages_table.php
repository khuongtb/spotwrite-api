<?php

use Illuminate\Database\Migrations\Migration;
use BaoPham\DynamoDb\DynamoDbClientService;

class CreateDynamodbMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // テスト時には DynamoDB を利用しない
        if (app()->environment('testing')) {
            return;
        }

        $params = [
            'TableName' => 'Messages',
            'KeySchema' => [
                [
                    'AttributeName' => 'room_uuid',
                    'KeyType' => 'HASH',
                ],
                [
                    'AttributeName' => 'chat_message_identifier',
                    'KeyType' => 'RANGE',
                ],
            ],
            'AttributeDefinitions' => [
                [
                    'AttributeName' => 'room_uuid',
                    'AttributeType' => 'S'
                ],
                [
                    'AttributeName' => 'chat_message_identifier',
                    'AttributeType' => 'S'
                ],
            ],
            // キャパシティユニットは仮の値を入れておくだけ
            'ProvisionedThroughput' => [
                'ReadCapacityUnits' => 5,
                'WriteCapacityUnits' => 5,
            ],
        ];

        resolve(DynamoDbClientService::class)->getClient()->createTable($params);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // テスト時には DynamoDB を利用しない
        if (app()->environment('testing')) {
            return;
        }

        $params = [
            'TableName' => 'Messages',
        ];

        resolve(DynamoDbClientService::class)->getClient()->deleteTable($params);
    }
}
