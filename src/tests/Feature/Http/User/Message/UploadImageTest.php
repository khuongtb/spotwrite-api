<?php

namespace Tests\Feature\Http\User\Message;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Entities\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

/**
 * Class UploadImage
 * @package Tests\Feature\Http\User\Message
 */
class UploadImageTest extends TestCase
{
    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function 画像の保存に成功する(): void
    {
        Storage::fake(config('filesystems.default'));

        $user = factory(User::class)->create();
        $this->authorizeWithJwt($user);

        $request = [
            'image' => UploadedFile::fake()->image('sample.jpg'),
        ];

        $response = $this->postJson(route('message.store.uploadImage'), $request);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(['thumbnail_url', 'main_image_url']);

        Storage::disk(config('filesystems.default'))
            ->assertExists(explode("/storage", $response->json()['thumbnail_url'])[1]);
        Storage::disk(config('filesystems.default'))
            ->assertExists(explode("/storage", $response->json()['main_image_url'])[1]);
    }
}
