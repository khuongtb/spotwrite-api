<?php

namespace Tests\Feature\Http\User\Message;

use App\Entities\Models\MessageRoom;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use App\Entities\Models\User;

/**
 * Class UploadImage
 * @package Tests\Feature\Http\User\Message
 */
class RoomTest extends TestCase
{
    use RefreshDatabase;

    private User $user;

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = factory(User::class)->create();
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function ROOMの作成に成功する(): void
    {
        $this->authorizeWithJwt($this->user);

        $request = [
            'user_uuids' => [
                $this->user->uuid,
            ]
        ];
        $response = $this->postJson(route('message.room.store'), $request);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(['room_uuid']);
        $responseBody = json_decode($response->getContent());

        $this->assertDatabaseHas('message_rooms', [
            'uuid' => $responseBody->room_uuid,
        ]);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function ROOMの作成に失敗(): void
    {
        $this->authorizeWithJwt($this->user);

        $request = [
            'user_uuids' => [
                'c955bddc-1183-410b-8579-149edb446b3b',
            ]
        ];
        $response = $this->postJson(route('message.room.store'), $request);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function 自分が所属するメッセージルームの一覧を取得(): void
    {
        $messageRoom = factory(MessageRoom::class)->create();
        $this->authorizeWithJwt($this->user);
        $messageRoom->users()->save($this->user);

        $response = $this->getJson(route('message.room.index'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'rooms' => [
                ['room_uuid' => $messageRoom->uuid,]
            ]
        ]);
    }
}
