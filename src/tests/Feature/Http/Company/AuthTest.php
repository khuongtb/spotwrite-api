<?php

namespace Tests\Feature\Http\Company;

use App\Entities\Models\AccountHolder;
use App\Entities\Models\Company;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

/**
 * Class AuthTest
 * @package Tests\Feature\Http\Company
 */
class AuthTest extends TestCase
{
    use RefreshDatabase;

    private AccountHolder $accountHolder;

    private Company $company;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->company = factory(Company::class)->create();
        $this->accountHolder = factory(AccountHolder::class)->create([
            'company_id' => $this->company->id,
            'password' => 'password',
        ]);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function ログインするとJWTトークンが返る(): void
    {
        $request = [
            'email' => $this->accountHolder->email,
            'password' => 'password',
        ];
        $response = $this->postJson(route('company.signin'), $request);

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson([
            'token_type' => 'bearer',
            'expires_in' => 3600,
        ]);
        $response->assertJsonStructure([
            'access_token',
            'token_type',
            'expires_in',
        ]);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function ログインに失敗すると401が返る(): void
    {
        // emailが未指定
        $request = [
            'password' => 'password',
        ];
        $response = $this->postJson(route('company.signin'), $request);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);

        // emailが未登録
        $request = [
            'email' => 'test@test.com',
            'password' => 'password',
        ];
        $response = $this->postJson(route('company.signin'), $request);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);

        // パスワードが誤っている
        $request = [
            'email' => $this->accountHolder->email,
            'password' => 'incorrect-password',
        ];
        $response = $this->postJson(route('company.signin'), $request);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function ログアウトできる(): void
    {
        $request = [
            'email' => $this->accountHolder->email,
            'password' => 'password',
        ];
        $this->postJson(route('company.signin'), $request);
        $response = $this->postJson(route('company.signout'));
        $response->assertStatus(Response::HTTP_OK);
    }

    /**
     * @noinspection NonAsciiCharacters
     * @test
     */
    public function 未ログイン状態でログアウトすると401エラーになる(): void
    {
        $response = $this->postJson(route('company.signout'));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
